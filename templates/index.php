<?
	$authorized = $this->checkRights();
?>
<!doctype html>
<html lang="ru">
<head>
	<title>Tasker</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta charset="utf-8">
	<meta http-equiv="Cache-Control" content="no-cache">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&amp;subset=cyrillic" rel="stylesheet"> 
	<link rel="stylesheet" href="/static/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/static/css/style.css?<?=time();?>">	
</head>
<body>
	<div class="container">
		<div class="row justify-content-end mt-3">
			<? if(!$authorized){?>
				<a href="/login" class="btn btn-primary">Войти</a>			
			<?}else{?>
				<a href="javascript:void(0)" class="btn btn-warning logout">Выйти</a>
			<?}?>
		</div>
		<h1>
			Tasker
		</h1>
	</div>
	<div class="container">
		<form action="" class="row sortForm sort_form">
			<div class="form-group col">
				<div class="sort_form_label">
					Сортировать по:
				</div>
				<select name="orderby" class="form-control">
					<option value="name" <?=(!isset($_REQUEST['orderby']) || $_REQUEST['orderby'] == 'name') ? 'selected' : '' ;?>>Имя пользователя</option>
					<option value="email" <?=($_REQUEST['orderby'] == 'email') ? 'selected' : '' ;?>>Email</option>
					<option value="is_complete" <?=($_REQUEST['orderby'] == 'is_complete') ? 'selected' : '' ;?>>Статус</option>
				</select>
			</div>
			<div class="form-group col">
				<div class="sort_form_label">
					Сортировать как:
				</div>
				<select name="order" class="form-control">
					<option value="ASC" <?=(!isset($_REQUEST['order']) || $_REQUEST['order'] == 'ASC') ? 'selected' : '' ;?>>По возрастанию</option>
					<option value="DESC" <?=($_REQUEST['order'] == 'DESC') ? 'selected' : '' ;?>>По убыванию</option>
				</select>
			</div>
			<? if($result['pag'] > 1){?>
				<div class="w-100"></div>
				<div class="col pag_block">
					<div class="sort_form_label">
						Страницы:
					</div>
					<div class="pag_wrap">
						<?for($i = 1; $i <= $result['pag']; $i++) {
							$checked = '';
							if ($i == 1) {
								if ((!isset($_REQUEST['pagenum']) || $_REQUEST['pagenum'] == 1)) {
									$checked = 'checked';
								}
							} else {
								if ($_REQUEST['pagenum'] == $i) {
									$checked = 'checked';
								}
							}
							?>
							<input class="pag_check" type="radio" name="pagenum" value="<?=$i;?>" <?=$checked;?> id="pagnum<?=$i;?>">
							<label class="pag_item" for="pagnum<?=$i;?>">
								<?=$i;?>
							</label>						
						<?}?>
					</div>
				</div>					
			<?}?>
		</form>
		<div class="row tasks">
			<?foreach ($result['items'] as $row) {?>				
				<div class="col-12 col-lg-4 col-sm-6 task_wrap">
					<div class="task" data-id="<?=$row['id'];?>">
						<div>Имя: <span><?=htmlentities($row['name']);?></span></div>
						<div>Email: <span><?=htmlentities($row['email']);?></span></div>					
						<div>Статус: 
							<span>
								<select name="is_complete" <? if(!$authorized){?>readonly disabled <?}else{?>class="custom-select"<?}?>>
									<option value="0" <?=$row['is_complete'] == 0 ? 'selected' : '';?>>Не выполнена</option>
									<option value="1" <?=$row['is_complete'] == 1 ? 'selected' : '';?>>Выполнена</option>
								</select>
							</span>
						</div>
						<div>Email: <span><?=htmlentities($row['email']);?></span></div>
						<div>
							Описание: <br>
							<span><textarea name="desc" class="w-100 form-control" <? if(!$authorized){?>readonly disabled <?}?>><?=htmlentities($row['desc']);?></textarea></span>
						</div>
						<div class="adm_edit <?=$row['is_edited'] == 0 ? 'd-none' : '';?>">Отредактировано администратором</div>
						<? if($authorized){?>
							<div class="container">
								<div class="row justify-content-end">
									<button class="btn btn-success editTask">Сохранить</button>
									<div class="w-100 mt-3 alert d-none"></div>
								</div>
							</div>
						<?}?>
					</div>
				</div>
			<?}?>
		</div>
	</div>
	<br><br>
	<div class="container mb-5">
		<div class="add_task pt-5">
			<h3 class="mb-3">Добавить задачу</h3>
			<form method="POST" class="addForm" action="">
				<input type="hidden" name="type" value="add">
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<input type="text" name="name" class="form-control" required placeholder="Ваше имя">
						<div class="invalid-feedback">Введите имя</div>
					</div>
					<div class="col-md-6 mb-3">
						<input type="email" name="email" class="form-control" required placeholder="Ваше email">
						<div class="invalid-feedback">Введите корректный email</div>
					</div>
				</div>
				<div class="form-row">
					<textarea name="desc" rows="5" placeholder="Описание задачи" class="form-control mb-3" required autocomplete="off"></textarea>
					<div class="invalid-feedback">Введите текст задачи</div>
				</div>
				<div class="form-row">
					<div class="alert alert-danger d-none" role="alert" id="dbErr">
						Ошибка сохранения, попробуйте позже
					</div>
				</div>
				<div class="form-row justify-content-end">
					<input class="btn btn-success" id="addFormBtn" type="submit" value="Добавить">
				</div>
			</form>
		</div>		
	</div>
	<script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
	<script type="text/javascript" src="/static/js/script.js?<?=time();?>"></script>
</body>
</html>