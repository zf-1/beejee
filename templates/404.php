<?
	$authorized = $this->checkRights();
	header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found"); 
?>
<!doctype html>
<html lang="ru">
<head>
	<title>Страница не найдена: Tasker</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta charset="utf-8">
	<meta http-equiv="Cache-Control" content="no-cache">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&amp;subset=cyrillic" rel="stylesheet"> 
	<link rel="stylesheet" href="/static/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/static/css/style.css?<?=time();?>">	
</head>
<body>
	<div class="container mt-5">
		<h1>
			404 Страница не найдена
		</h1>
		<a href="/" class="btn btn-primary">На главную</a>
	</div>
	
	
	<script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
	<script type="text/javascript" src="/static/js/script.js?<?=time();?>"></script>
</body>
</html>