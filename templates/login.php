<?
	$authorized = $this->checkRights();
?>
<!doctype html>
<html lang="ru">
<head>
	<title>Авторизация: Tasker</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta charset="utf-8">
	<meta http-equiv="Cache-Control" content="no-cache">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&amp;subset=cyrillic" rel="stylesheet"> 
	<link rel="stylesheet" href="/static/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/static/css/style.css?<?=time();?>">	
</head>
<body>
	<div class="container mt-5">
		<? if($authorized){?>
			<h1>
				Вы успешно авторизировались!
			</h1>		
			<a href="/" class="btn btn-primary">На главную</a>
			<a href="javascript:void(0)" class="btn btn-warning logout">Выйти</a>
		<?}else{?>
			<h1 class="mb-3">
				Авторизация
			</h1>
			<form action="" class="loginForm" method="POST">
				<input type="hidden" name="type" value="login">
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<input type="text" name="login" class="form-control" required placeholder="Логин">
						<div class="invalid-feedback">Поле не долджно быть пустым</div>
					</div>
					<div class="col-md-6 mb-3">
						<input type="password" name="password" class="form-control" required placeholder="Пароль">
						<div class="invalid-feedback">Поле не долджно быть пустым</div>
					</div>
				</div>
				<div class="form-row">
					<div class="alert alert-danger d-none" role="alert" id="loginErr">
						
					</div>
				</div>
				<div class="form-row justify-content-end">
					<input class="btn btn-success" id="loginBtn" type="submit" value="Войти">
				</div>
			</form>
		<?}?>
		
	</div>
	
	
	<script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
	<script type="text/javascript" src="/static/js/script.js?<?=time();?>"></script>
</body>
</html>