$(function () {
	
	$('.sortForm input, .sortForm select').on('change',function(e){
		$(this).closest('form').submit();
	})

	$('.addForm').on('submit',function(event){
		event.preventDefault();
		var form = $(this);
		$.post('/app/ajax.php', $(this).serialize(), function (data) {
			if (!$.isEmptyObject(data)) {
				$.each(data, function(index, value) {
					if (index != 'BD') {
						form.find('[name="' + index + '"]').addClass('is-invalid');
					} else {
						$('#dbErr').removeClass('d-none');
					}
				}); 
			} else {
				form.html('<div class="alert alert-success" role="alert">Задача добавлена</div>');
				setTimeout(() => {
					location.reload();
				}, 2000);
			}
			
		},'json')
	})
	$('.editTask').on('click', function (event) {
		event.preventDefault();
		var	_this = $(this),
			form = _this.closest('.task'),
			id = form.data('id'),
			is_complete = form.find('[name="is_complete"]').val(),
			desc = form.find('[name="desc"]').val();
		$.post('/app/ajax.php', { type: 'update', id: id, is_complete: is_complete, desc: desc }, function (data) {
			if (data != 'ok') {
				_this.siblings('.alert').removeClass('alert-success d-none').addClass('alert-danger').html(data);
			} else {
				_this.siblings('.alert').removeClass('alert-danger d-none').addClass('alert-success').html('Изменения сохранены');
				form.find('.adm_edit').removeClass('d-none');
			}
			
		},'json')
	})

	$('.loginForm').on('submit',function(event){
		event.preventDefault();
		var form = $(this);
		$.post('/app/ajax.php', form.serialize(), function (data) {
			if (data != 'ok') {
				$('#loginErr').removeClass('d-none').text(data);
			} else {
				location.reload();
			}
			
		})
	})

	$('.logout').on('click',function(e){
		$.post('/app/ajax.php', {type: 'logout'}, function (data) {
			location.reload();			
		})
	})
	
})

