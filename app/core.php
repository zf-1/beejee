<?
class Core{
	private $db;
	const lim = 3;
	function __construct(){
		$this->db = new PDO('sqlite:'.$_SERVER['DOCUMENT_ROOT'].'/app/db.db');
	}
	function getAll($pagenum = 1, $order = 'name', $dir = 'ASC'){
		$offset = ($pagenum > 0) ? ($pagenum - 1) * static::lim : $pagenum * static::lim;
		$st = $this->db->query('SELECT * FROM tasks ORDER BY '.$order.' '.$dir.' LIMIT '.static::lim.' OFFSET '.$offset);
		$results = $st->fetchAll(PDO::FETCH_ASSOC); 
		$st->closeCursor();
		return $results;
	}
	function getPag(){
		$st = $this->db->query('SELECT COUNT(*) FROM tasks');
		$results = $st->fetchColumn(); 
		$st->closeCursor();
		return ceil($results / static::lim);
	}
	function updateTask($id,$is_complete, $desc){
		if($st = $this->db->prepare('UPDATE tasks SET is_edited = 1, is_complete = :is_complete, desc = :desc WHERE id = :id')){			
			if ($st->execute([':is_complete' => $is_complete,':desc' => $desc,':id' => $id])) {
				return true;
			} else {
				return false;
			}			
		}else{
			return false;
		}
	}
	
	function addTask($name, $email, $desc){
		if($st = $this->db->prepare('INSERT INTO  tasks (name,email,desc) VALUES (:name, :email, :desc)')){			
			if ($st->execute([':name' => $name,':email' => $email,':desc' => $desc])) {
				return true;
			} else {
				return false;
			}			
		}else{
			return false;
		}
	}

	function getUser($login){
		if($st = $this->db->prepare('SELECT user_pass FROM users WHERE user_name = :login')){			
			if ($st->execute([':login' => $login])) {
				return $st->fetch(PDO::FETCH_ASSOC);
			} else {
				return false;
			}			
		}else{
			return false;
		}
	}
	
}

?>