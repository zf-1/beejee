<? 
require_once('core.php');
session_start();
class App
{
	protected $model;
	private static $_instance = null;
	private function __construct()
	{
		$this->model = new Core;
	}

	public static function getInstance()
	{
		if (self::$_instance != null) {
			return self::$_instance;
		}

		return new self;
	}

	function showPage(){
		$result = [];
		$result['items'] = $this->model->getAll(intval($_REQUEST['pagenum']), ($_REQUEST['orderby']) ?? 'name', ($_REQUEST['order']) ?? 'ASC');
		$result['pag'] = $this->model->getPag();
		$page = ($_REQUEST['page']) ?? 'index';
		if (file_exists($_SERVER['DOCUMENT_ROOT'].'/templates/'.$page.'.php')) {
			require($_SERVER['DOCUMENT_ROOT'].'/templates/'.$page.'.php');
		} else {
			require($_SERVER['DOCUMENT_ROOT'].'/templates/404.php');
		}
	}

	function addTask($name, $email, $desc){
		$name = strip_tags($name);
		$email = strip_tags($email);
		$desc = strip_tags($desc);
		return $this->model->addTask($name, $email, $desc);
	}

	function checkRights(){
		return !(!$_SESSION['logged'] || $_SESSION['logged'] == 0);
	}

	function updateTask($id, $is_complete,$desc){
		$id = intval($id);
		$is_complete = intval($is_complete);
		return $this->model->updateTask($id, $is_complete, $desc);
	}

	function authorize($login, $password){
		if($user = $this->model->getUser($login)){
			if ($user['user_pass'] == $password) {
				$_SESSION['logged'] = 1;
				return true;
			} else {
				return false;
			}			
		}else{
			return false;
		}
	}
}

