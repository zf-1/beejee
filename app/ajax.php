<?
require './app.php';
$app = App::getInstance();
if ($_POST['type']) {
	if ($_POST['type'] == 'add') {
		$errors = [];
		foreach ($_POST as $key => $value) {
			if (trim($value) == '') {
				$errors[$key] = 'Заполните это поле';
			}elseif ($key == 'email' && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
				$errors[$key] = 'Введите корректный email';
			}
		}
		if (!count($errors)) {
			if ($app->addTask($_POST['name'], $_POST['email'], $_POST['desc'])) {
				echo json_encode([]);
			} else {
				echo json_encode(['BD' => 'Ошибка сохранения']);
			}			
		} else {
			echo json_encode($errors);
		}		
	}

	if ($_POST['type'] == 'update') {
		if ($app->checkRights()) {
			if ($app->updateTask($_POST['id'], $_POST['is_complete'], $_POST['desc'])) {
				echo json_encode('ok');
			} else {
				echo json_encode('Ошибка сохранения');
			}
		} else {
			echo json_encode('Недостаточно прав');
		}
	}

	
	if ($_POST['type'] == 'login') {
		$login = trim($_POST['login']);
		$result = ($app->authorize($login, md5($_POST['password']))) ? 'ok' : 'неверный логин или пароль';
		echo $result;
	}	

	if ($_POST['type'] == 'logout') {
		$_SESSION['logged'] = 0;
	}

}